package com.itau.atividadeApiJava.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.atividadeApiJava.models.Customer;
import com.itau.atividadeApiJava.models.Mensagem;
import com.itau.atividadeApiJava.models.Saque;
import com.itau.atividadeApiJava.repositories.ClienteRepository;
import com.itau.atividadeApiJava.services.RatesConverter;

@RestController
public class CustomerController {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	RatesConverter conversor;

	@RequestMapping(method=RequestMethod.POST, path="/cliente/incluir")
	public ResponseEntity<Customer> incluirCliente(@Valid @RequestBody Customer cliente ) {
		Customer c = clienteRepository.save(cliente);
		return ResponseEntity.ok().body(c);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/clientes")
	public Iterable<Customer> getClientes() {
		return clienteRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/cliente/sacar")
	public ResponseEntity<?> sacar(@Valid @RequestBody Saque saque) {
		Customer cliente = clienteRepository.findByUsername(saque.getUsername());
		double valorConvertido = conversor.converter(saque.getMoeda(), saque.getValor());
		double saldoRemanescente = cliente.getBalance() - valorConvertido;
		if (saldoRemanescente <= 0.1) {
			Mensagem mensagem = new Mensagem();
			mensagem.mensagem = "Saldo insuficiente";
			return ResponseEntity.status(401).body(mensagem);
		}
		cliente.setBalance(saldoRemanescente);
		clienteRepository.save(cliente);
		return ResponseEntity.ok(cliente);
	}
	
	
}
