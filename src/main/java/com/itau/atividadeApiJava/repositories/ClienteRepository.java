package com.itau.atividadeApiJava.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.atividadeApiJava.models.Customer;

public interface ClienteRepository extends CrudRepository<Customer, Long>{
	public Customer findByUsername(String username);
}
