package com.itau.atividadeApiJava.services;

import java.text.MessageFormat;
import java.util.HashMap;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.atividadeApiJava.models.RatesResponse;

@Service
public class RatesConverter {

	RestTemplate restTemplate = new RestTemplate();
	
	public double converter(String moeda, double valor) {
		String url = MessageFormat.format("http://data.fixer.io/api/latest?access_key=19d13a140834c1241593ec64a895403d&base=EUR&symbols={0}", moeda);
		
		RatesResponse rates = restTemplate.getForObject(url, RatesResponse.class);
		HashMap<String, Double> cotacaoEuro = rates.getRates();
		double valorConvertido = valor / cotacaoEuro.get(moeda);
		return valorConvertido;
	}
	
}
